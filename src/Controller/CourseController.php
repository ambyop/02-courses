<?php

namespace App\Controller;

use App\Repository\CourseCategoryRepository;
use App\Repository\CourseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CourseController
 */
class CourseController extends AbstractController
{
    /**
     * @Route("/courses", name="courses")
     * @param CourseCategoryRepository $categoryRepository
     * @param CourseRepository $courseRepository
     * @return Response
     */
    public function courses(CourseCategoryRepository $categoryRepository, CourseRepository $courseRepository) : Response
    {
        $categories = $categoryRepository->findBy(
            [],
            ['name' => 'ASC']
        );
        $courses = $courseRepository->findBy(
            ['isPublished' => true],
            ['name' => 'ASC']
        );



        return $this->render('course/courses.html.twig', [
            'categories' => $categories,
            'courses' => $courses
        ]);
    }

    /**
     * @Route("/course/{slug}", name="course")
     * @param CourseRepository $courseRepository
     * @param string $slug
     * @return Response
     */
    public function course(CourseRepository $courseRepository, string $slug) : Response
    {
        $course = $courseRepository->findOneBy(
            ['slug' => $slug]
        );

        return $this->render('course/detail.html.twig',[
            'course' => $course
        ]);

    }
}
